package com.example.bob.hiit.utils

import android.app.Application
import com.example.bob.hiit.api.ApiService
import com.example.bob.hiit.api.RetrofitLibrary
import com.example.bob.hiit.repository.MainRepository
import com.example.bob.hiit.repository.MainRepositoryImplementation
import com.example.bob.hiit.viewModel.MainViewModel
import org.koin.android.ext.android.startKoin
import org.koin.android.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.module
import retrofit2.Retrofit

class App:Application() {


    companion object getInstance{
        @get:Synchronized
        var instance: App? = null
    }


    private val modules=module{
        single { Settings(get()) }
        single ("retrofit"){RetrofitLibrary().initClient(get())}
        single { (get("retrofit") as Retrofit).create(ApiService::class.java)as ApiService }
        single { MainRepositoryImplementation(get()) as MainRepository}
        viewModel{ MainViewModel(get()) }

    }


    override fun onCreate() {
        super.onCreate()
        instance=this
        startKoin(this, listOf(modules))

    }

    fun setConnectivityListener(listener:ConnectivityReceiver.ConnectivityReceiverListener){
        ConnectivityReceiver.connectivityReceiver=listener
    }
}