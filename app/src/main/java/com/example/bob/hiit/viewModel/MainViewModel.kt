package com.example.bob.hiit.viewModel

import android.app.AlertDialog
import android.content.Context
import android.graphics.Color
import android.view.Gravity
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.LinearLayout
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.bob.hiit.model.*
import com.example.bob.hiit.repository.MainRepository
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainViewModel(var mainRepository: MainRepository) : ViewModel() {

    val isResponseSuccessful = MutableLiveData<Boolean>()
    val registrationResponse = MutableLiveData<User>()
    val productResponseResult = MutableLiveData<ProductResponse>()
    val categoryResponse = MutableLiveData<CategoryResponse>()
    val category = MutableLiveData<Category>()
    val exercisesResponse = MutableLiveData<ExerciseResponse>()
    val exercise = MutableLiveData<Exercise>()
    val user = MutableLiveData<User>()
    val usersList = MutableLiveData<UserList>()
    val subscriptionTypeList = MutableLiveData<SubscriptionTypeList>()
    val subscriptionsList = MutableLiveData<SubscriptionsList>()
    val subscription = MutableLiveData<Subscription>()


    fun userRegistration(userUser: User, context: Context) {
        val dialog = setProgressDialog(context, "Requesting...")
        dialog.show()
        mainRepository.userRegistration(userUser)
            .enqueue(object : Callback<User> {
                /**
                 * Invoked when a network exception occurred talking to the server or when an unexpected
                 * exception occurred creating the request or processing the response.
                 */
                override fun onFailure(call: Call<User>, t: Throwable) {
                    dialog.cancel()
                }

                /**
                 * Invoked for a received HTTP response.
                 *
                 *
                 * Note: An HTTP response may still indicate an application-level failure such as a 404 or 500.
                 * Call [Response.isSuccessful] to determine if the response indicates success.
                 */
                override fun onResponse(call: Call<User>, user: Response<User>) {
                    if (user.isSuccessful) {
                        dialog.cancel()
                        isResponseSuccessful.postValue(user.isSuccessful)
                    } else {
                        dialog.cancel()
                        isResponseSuccessful.postValue(false)
                    }
                }
            })
    }


    fun userAuthentication(username: String, password: String, context: Context) {
        val dialog = setProgressDialog(context, "Retrieving...")
        dialog.show()
        mainRepository.userAuthentication(username, password)
            .enqueue(object : Callback<User> {
                /**
                 * Invoked when a network exception occurred talking to the server or when an unexpected
                 * exception occurred creating the request or processing the response.
                 */
                override fun onFailure(call: Call<User>, t: Throwable) {
                    dialog.cancel()
                }

                /**
                 * Invoked for a received HTTP response.
                 *
                 *
                 * Note: An HTTP response may still indicate an application-level failure such as a 404 or 500.
                 * Call [Response.isSuccessful] to determine if the response indicates success.
                 */
                override fun onResponse(
                    call: Call<User>,
                    user: Response<User>
                ) {
                    if (user.isSuccessful) {
                        dialog.cancel()
                        registrationResponse.postValue(user.body())
                    } else {
                        dialog.cancel()
                        Toast.makeText(context, "unsuccessful response", Toast.LENGTH_SHORT).show()
                    }
                }

            })
    }


    fun userUpdate(userId: Int, user: User, context: Context) {
        val dialog = setProgressDialog(context, "Updating...")
        dialog.show()

        mainRepository.userUpdate(userId, user).enqueue(object : Callback<User> {
            /**
             * Invoked when a network exception occurred talking to the server or when an unexpected
             * exception occurred creating the request or processing the response.
             */
            override fun onFailure(call: Call<User>, t: Throwable) {
                dialog.cancel()
            }

            /**
             * Invoked for a received HTTP response.
             *
             *
             * Note: An HTTP response may still indicate an application-level failure such as a 404 or 500.
             * Call [Response.isSuccessful] to determine if the response indicates success.
             */
            override fun onResponse(call: Call<User>, response: Response<User>) {
                if (response.isSuccessful) {
                    dialog.cancel()
                    isResponseSuccessful.postValue(true)
                } else {
                    dialog.cancel()
                    isResponseSuccessful.postValue(false)
                }
            }

        })
    }

    fun getUserDetails() {
        mainRepository.getUserDetails().enqueue(object : Callback<User> {
            override fun onFailure(call: Call<User>, t: Throwable) {
                user.value = null
            }

            override fun onResponse(call: Call<User>, response: Response<User>) {
                if (response.isSuccessful)
                    user.value = response.body()
            }

        })
    }


    fun getProducts(context: Context) {
        val dialog = setProgressDialog(context, "retrieving products...")
        dialog.show()

        mainRepository.getProducts().enqueue(object : Callback<ProductResponse> {
            /**
             * Invoked when a network exception occurred talking to the server or when an unexpected
             * exception occurred creating the request or processing the response.
             */
            override fun onFailure(call: Call<ProductResponse>, t: Throwable) {
                dialog.cancel()
            }

            /**
             * Invoked for a received HTTP response.
             *
             *
             * Note: An HTTP response may still indicate an application-level failure such as a 404 or 500.
             * Call [Response.isSuccessful] to determine if the response indicates success.
             */
            override fun onResponse(
                call: Call<ProductResponse>,
                response: Response<ProductResponse>
            ) {
                dialog.cancel()
                if (response.isSuccessful) {
                    productResponseResult.postValue(response.body())
                }
            }

        })
    }

    fun getCategoryList(context: Context) {
        val dialog = setProgressDialog(context, "retrieving categories...")
        dialog.show()

        mainRepository.getCategoryList().enqueue(object : Callback<CategoryResponse> {
            override fun onFailure(call: Call<CategoryResponse>, t: Throwable) {
                dialog.cancel()
            }

            override fun onResponse(
                call: Call<CategoryResponse>,
                response: Response<CategoryResponse>
            ) {
                dialog.cancel()
                if (response.isSuccessful) {
                    categoryResponse.value = response.body()
                }
            }

        })
    }

    fun getCategoryById(context: Context, id: Int) {
        val dialog = setProgressDialog(context, "retrieving category...")
        dialog.show()

        mainRepository.getCategoryById(id).enqueue(object : Callback<Category> {
            override fun onFailure(call: Call<Category>, t: Throwable) {
                dialog.cancel()
            }

            override fun onResponse(call: Call<Category>, response: Response<Category>) {
                dialog.cancel()
                if (response.isSuccessful) {
                    category.value = response.body()
                }
            }

        })
    }

    fun getAllExercises() {
        mainRepository.getAllExercises().enqueue(object : Callback<ExerciseResponse> {
            override fun onFailure(call: Call<ExerciseResponse>, t: Throwable) {
            }

            override fun onResponse(
                call: Call<ExerciseResponse>,
                response: Response<ExerciseResponse>
            ) {
                if (response.isSuccessful) {
                    exercisesResponse.value = response.body()
                }
            }

        })
    }

    fun addExercise(context: Context, ex: PostExercise) {
        val dialog = setProgressDialog(context, "adding exercise...")
        dialog.show()

        mainRepository.addExercise(ex).enqueue(object : Callback<Exercise> {
            override fun onFailure(call: Call<Exercise>, t: Throwable) {
                dialog.cancel()
            }

            override fun onResponse(call: Call<Exercise>, response: Response<Exercise>) {
                dialog.dismiss()
                if (response.isSuccessful)
                    exercise.value = response.body()
            }

        })
    }

    fun editExercise(context: Context, id: Int, ex: PostExercise) {
        val dialog = setProgressDialog(context, "editing exercise...")
        dialog.show()

        mainRepository.editExercise(id, ex).enqueue(object : Callback<Exercise> {
            override fun onFailure(call: Call<Exercise>, t: Throwable) {
                dialog.cancel()
            }

            override fun onResponse(call: Call<Exercise>, response: Response<Exercise>) {
                dialog.dismiss()
                if (response.isSuccessful)
                    exercise.value = response.body()
            }
        })
    }


    fun deleteExercise(context: Context, id: Int) {
        val dialog = setProgressDialog(context, "deleting exercise...")
        dialog.show()

        mainRepository.deleteExercise(id).enqueue(object : Callback<Void> {
            override fun onFailure(call: Call<Void>, t: Throwable) {
                dialog.cancel()
            }

            override fun onResponse(call: Call<Void>, response: Response<Void>) {
                isResponseSuccessful.value = response.isSuccessful
                dialog.cancel()
            }

        })
    }

    fun getAllUsers() {
        mainRepository.getAllUsers().enqueue(object : Callback<UserList> {
            override fun onFailure(call: Call<UserList>, t: Throwable) {
                usersList.value = null
            }

            override fun onResponse(call: Call<UserList>, response: Response<UserList>) {
                if (response.isSuccessful)
                    usersList.value = response.body()
            }

        })
    }

    fun getAllSubsType() {
        mainRepository.getAllSubsType().enqueue(object : Callback<SubscriptionTypeList> {
            override fun onFailure(call: Call<SubscriptionTypeList>, t: Throwable) {
                subscriptionTypeList.value = null
            }

            override fun onResponse(
                call: Call<SubscriptionTypeList>,
                response: Response<SubscriptionTypeList>
            ) {
                if (response.isSuccessful)
                    subscriptionTypeList.value = response.body()
            }

        })
    }

    fun getAllSubs() {
        mainRepository.getAllSubs().enqueue(object : Callback<SubscriptionsList> {
            override fun onFailure(call: Call<SubscriptionsList>, t: Throwable) {
                subscriptionsList.value = null
            }

            override fun onResponse(
                call: Call<SubscriptionsList>,
                response: Response<SubscriptionsList>
            ) {
                if (response.isSuccessful)
                    subscriptionsList.value = response.body()
            }

        })
    }

    fun addNewSub(context: Context, sub: SubscriptionPost) {
        val dialog = setProgressDialog(context, "Adding subscription...")
        dialog.show()
        mainRepository.addNewSub(sub).enqueue(object : Callback<Void> {
            override fun onFailure(call: Call<Void>, t: Throwable) {
                isResponseSuccessful.value = false
                dialog.cancel()
            }

            override fun onResponse(call: Call<Void>, response: Response<Void>) {
                if (response.isSuccessful)
                    isResponseSuccessful.value = true
                dialog.cancel()
            }

        })
    }

    fun getSubDetailById(id: Int) {
        mainRepository.getSubDetailById(id).enqueue(object : Callback<Subscription> {
            override fun onFailure(call: Call<Subscription>, t: Throwable) {
                subscription.value = null
            }

            override fun onResponse(call: Call<Subscription>, response: Response<Subscription>) {
                if (response.isSuccessful)
                    subscription.value = response.body()
            }

        })
    }


    fun setProgressDialog(context: Context, message: String): AlertDialog {
        val llPadding = 30
        val ll = LinearLayout(context)
        ll.orientation = LinearLayout.HORIZONTAL
        ll.setPadding(llPadding, llPadding, llPadding, llPadding)
        ll.gravity = Gravity.CENTER
        var llParam = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.WRAP_CONTENT,
            LinearLayout.LayoutParams.WRAP_CONTENT
        )
        llParam.gravity = Gravity.CENTER
        ll.layoutParams = llParam

        val progressBar = ProgressBar(context)
        progressBar.isIndeterminate = true
        progressBar.setPadding(0, 0, llPadding, 0)
        progressBar.layoutParams = llParam

        llParam = LinearLayout.LayoutParams(
            ViewGroup.LayoutParams.WRAP_CONTENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )
        llParam.gravity = Gravity.CENTER
        val tvText = TextView(context)
        tvText.text = message
        tvText.setTextColor(Color.parseColor("#32A8C7"))
        tvText.textSize = 16.toFloat()
        tvText.layoutParams = llParam

        ll.addView(progressBar)
        ll.addView(tvText)

        val builder = AlertDialog.Builder(context)
        builder.setCancelable(true)
        builder.setView(ll)

        val dialog = builder.create()
        val window = dialog.window
        if (window != null) {
            val layoutParams = WindowManager.LayoutParams()
            layoutParams.copyFrom(dialog.window?.attributes)
            layoutParams.width = LinearLayout.LayoutParams.WRAP_CONTENT
            layoutParams.height = LinearLayout.LayoutParams.WRAP_CONTENT
            dialog.window?.attributes = layoutParams
        }
        return dialog
    }


}