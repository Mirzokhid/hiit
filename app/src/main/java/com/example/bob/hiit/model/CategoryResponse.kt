package com.example.bob.hiit.model

import com.google.gson.annotations.SerializedName

class CategoryResponse {
    @SerializedName("count")
    var count: Int? = null

    @SerializedName("next")
    var next: String? = null

    @SerializedName("previous")
    var previous: String? = null

    @SerializedName("results")
    var categories: ArrayList<Category>? = null
}
