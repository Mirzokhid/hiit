package com.example.bob.hiit.api

import com.example.bob.hiit.model.*
import retrofit2.Call
import retrofit2.http.*

interface ApiService {

    @POST("api/v1/main/user/register/")
    fun userRegistration(@Body user: User): Call<User>

    @FormUrlEncoded
    @POST("api/v1/main/user/auth/")
    fun userAuthentication(
        @Field("username") username: String,
        @Field("password") password: String
    ): Call<User>

    @PUT("api/v1/main/user/{id}/")
    fun userUpdate(
        @Path("id") userId: Int,
        @Body user: User
    ): Call<User>

    @GET("/api/v1/main/user/info/")
    fun getUserDetails(): Call<User>

    @GET("/api/v1/main/product/")
    fun getProducts(): Call<ProductResponse>

    @GET("/api/v1/main/category/")
    fun getCategoryList(): Call<CategoryResponse>

    @GET("/api/v1/main/category/{id}/")
    fun getCategoryById(@Path("id") id: Int): Call<Category>

    @GET("/api/v1/main/exercise/")
    fun getAllExercises(): Call<ExerciseResponse>

    @POST("/api/v1/main/exercise/")
    fun addExercise(@Body exercise: PostExercise): Call<Exercise>

    @PUT("/api/v1/main/exercise/{id}/")
    fun editExercise(@Path("id") id: Int, @Body exercise: PostExercise): Call<Exercise>

    @DELETE("/api/v1/main/exercise/{id}/")
    fun deleteExercise(@Path("id") id: Int): Call<Void>

    @GET("/api/v1/main/user/")
    fun getAllUsers(): Call<UserList>

    @GET("/api/v1/main/subscription_type/")
    fun getAllSubsType(): Call<SubscriptionTypeList>

    @GET("/api/v1/main/subscription/")
    fun getAllSubs(): Call<SubscriptionsList>

    @POST("/api/v1/main/subscription/")
    fun addNewSub(@Body sub: SubscriptionPost): Call<Void>

    @GET("/api/v1/main/subscription/{id}/")
    fun getSubDetailById(@Path("id") id: Int): Call<Subscription>
}