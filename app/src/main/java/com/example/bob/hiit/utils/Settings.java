package com.example.bob.hiit.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.example.bob.hiit.model.User;

public class Settings {

    private static final String SHARED_PREF_NAME = "shared_preferences";
    private static Settings Instance;
    private Context mContext;
    private static SharedPreferences sharedPreferences;


    public static final String USER_ID = "user_id";
    public static final String USER_TOKEN = "user_token";
    public static final String USER_TYPE = "user_type";
    public static final String USER_NAME = "user_name";
    public static final String USER_CREDENTIALS = "user_credentials";
    public static final String COMPANY_TILE = "company_title";
    public static final String USER_IN = "user_in";
    public static final String BEGIN_DATE = "begin_date";
    public static final String END_DATE = "end_date";
//    public static final String USER_FULL_NAME = "user_full_name";


    public Settings(Context mCtx) {
        this.mContext = mCtx;
    }

    public static synchronized Settings getInstance(Context mCtx) {

        if (Instance == null) {
            sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
            Instance = new Settings(mCtx);
        }
        return Instance;
    }


    public void saveUser(int user_id, String user_token) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(USER_ID, user_id);
        editor.putString(USER_TOKEN, user_token);
        editor.apply();
    }

    public void saveCompanyTitle(String title) {
        sharedPreferences.edit().putString(COMPANY_TILE, title).apply();
    }

    public String getCompanyTitle() {
        return sharedPreferences.getString(COMPANY_TILE, null);
    }

    public void saveUserType(Boolean isCarrier) {
        sharedPreferences.edit().putBoolean(USER_TYPE, isCarrier).apply();
    }

    public void setIsUpdated(Boolean isUpdated) {
        sharedPreferences.edit().putBoolean(USER_CREDENTIALS, isUpdated).apply();
    }

    public Boolean getIsUpdated() {
        return sharedPreferences.getBoolean(USER_CREDENTIALS, false);
    }

    public Boolean getUserType() {
        return sharedPreferences.getBoolean(USER_TYPE, false);
    }

    public String getUserName() {
        return sharedPreferences.getString(USER_NAME, null);
    }

//    public void saveUserFullName(String fullName) {
//        sharedPreferences.edit().putString(USER_FULL_NAME, fullName).apply();
//    }
//
//    public String getUserFullName() {
//        return sharedPreferences.getString(USER_FULL_NAME, null);
//    }

    public void saveUserName(String name) {
        sharedPreferences.edit().putString(USER_NAME, name).apply();
    }

    public void saveUserId(int user_id) {
        sharedPreferences.edit().putInt(USER_ID, user_id).apply();
    }

    public int getUserId() {
        return sharedPreferences.getInt(USER_ID, -1);
    }

    public void saveUserToken(String user_token) {
        sharedPreferences.edit().putString(USER_TOKEN, user_token).apply();
    }

    public String getUserToken() {
        return sharedPreferences.getString(USER_TOKEN, "a3ef62642e6c2366fda8fa5f2039872a00ce864e");
    }


    public void setUserIn(Boolean isUserIn) {
        sharedPreferences.edit().putBoolean(USER_IN, isUserIn).apply();
    }

    public Boolean isUserIn() {
        return sharedPreferences.getBoolean(USER_IN, false);
    }

    public void setBeginDate(String date) {
        sharedPreferences.edit().putString(BEGIN_DATE, date).apply();
    }

    public String getBeginDate() {
        return sharedPreferences.getString(BEGIN_DATE, "");
    }

    public void setEndDate(String date) {
        sharedPreferences.edit().putString(END_DATE, date).apply();
    }

    public String getEndDate() {
        return sharedPreferences.getString(END_DATE, "");
    }

    public void clear() {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear();
        editor.apply();
    }

}
