package com.example.bob.hiit.exercise

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.*
import androidx.lifecycle.Observer
import com.example.bob.hiit.R
import com.example.bob.hiit.model.CategoryResponse
import com.example.bob.hiit.model.Exercise
import com.example.bob.hiit.model.PostExercise
import com.example.bob.hiit.viewModel.MainViewModel
import com.google.gson.Gson
import kotlinx.android.synthetic.main.admin_exercise_detail_edit_activity.*
import org.koin.android.ext.android.inject

class AdminExerciseDetail : AppCompatActivity(), AdapterView.OnItemSelectedListener {

    private var categoryID = -1
    private var selectedCategory = -1
    private lateinit var categoryList: ArrayList<String>
    private lateinit var categoryResponse: CategoryResponse
    private val viewModel: MainViewModel by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.admin_exercise_detail_edit_activity)

        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        categoryList = ArrayList()

        val bundle = intent.extras
        val exerciseJson = bundle!!.getString("exercise")
        val exercise = Gson().fromJson(exerciseJson, Exercise::class.java)

        tvExerciseDetailTitle.setText(exercise.title)
        tvExerciseDetailDes.setText(exercise.description)

        setTextFieldsEditable(false)
        categorySpinner.onItemSelectedListener = this

        viewModel.getCategoryList(this)
        viewModel.categoryResponse.observe(this, Observer {
            if (it != null) {
                categoryResponse = it
                for (i in 0 until it.categories!!.size) {
                    categoryList.add(it.categories!![i].title!!)
                    if (it.categories!![i].categoryId == exercise.category!!.categoryId)
                        categoryID = i
                }
                if (categoryList.isNotEmpty()) {
                    val adapter = ArrayAdapter(
                        this,
                        android.R.layout.simple_spinner_item,
                        categoryList
                    ).also {
                        it.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                    }

                    categorySpinner.adapter = adapter
                    if (categoryID != -1)
                        categorySpinner.setSelection(categoryID)
                }
            } else {
                Toast.makeText(this, "Error in getting Categories List", Toast.LENGTH_SHORT).show()
            }
        })

        editBtn.setOnClickListener {
            setTextFieldsEditable(true)
            editBtn.visibility = View.GONE
            saveBtn.visibility = View.VISIBLE
        }

        saveBtn.setOnClickListener {
            if (tvExerciseDetailTitle.text.isBlank()) {
                tvExerciseDetailTitle.error = "This field must not be empty"
                return@setOnClickListener
            }
            if (tvExerciseDetailDes.text.isBlank()) {
                tvExerciseDetailDes.error = "This field must not be empty"
                return@setOnClickListener
            }
            if (selectedCategory != -1) {
                val ex = PostExercise()
                ex.title = tvExerciseDetailTitle.text.toString()
                ex.description = tvExerciseDetailDes.text.toString()
                ex.category = selectedCategory

                viewModel.editExercise(this, exercise.id!!, ex)
                viewModel.exercise.observe(this, Observer {
                    if (it != null) {
                        editBtn.visibility = View.VISIBLE
                        saveBtn.visibility = View.GONE
                        Toast.makeText(this, "Exercise successfully updated", Toast.LENGTH_SHORT)
                            .show()
                        setTextFieldsEditable(false)
                    } else
                        Toast.makeText(
                            this,
                            "Something went wrong, try again",
                            Toast.LENGTH_SHORT
                        ).show()
                })
            }
        }

    }

    private fun setTextFieldsEditable(isEditable: Boolean) {
        if (isEditable) {
            setViewClickableFocusable(tvExerciseDetailTitle, true)
            setViewClickableFocusable(tvExerciseDetailDes, true)
            setViewClickableFocusable(categorySpinner, true)
        } else {
            setViewClickableFocusable(tvExerciseDetailTitle, false)
            setViewClickableFocusable(tvExerciseDetailDes, false)
            setViewClickableFocusable(categorySpinner, false)
        }

    }

    private fun setViewClickableFocusable(v: View, setClickable: Boolean) {
        if (setClickable) {
            v.isClickable = true
            v.isFocusable = true
            if (v is EditText) {
                v.isCursorVisible = true
                v.isFocusableInTouchMode = true
            }
            if (v is Spinner)
                v.isEnabled = true
        } else {
            v.isClickable = false
            v.isFocusable = false
            if (v is EditText) {
                v.isCursorVisible = false
                v.isFocusableInTouchMode = false
            }
            if (v is Spinner)
                v.isEnabled = false
        }
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        selectedCategory = categoryResponse.categories!![position].categoryId!!
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            this.finish()
        }
        return true
    }


}
