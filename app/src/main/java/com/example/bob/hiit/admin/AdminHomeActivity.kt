package com.example.bob.hiit.admin

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.ImageButton
import android.widget.Toast
import androidx.appcompat.widget.Toolbar
import androidx.core.view.GravityCompat
import com.example.bob.hiit.R
import com.google.android.material.navigation.NavigationView
import kotlinx.android.synthetic.main.activity_admin_home.*
import kotlinx.android.synthetic.main.start_activity.*

class AdminHomeActivity : AppCompatActivity(), View.OnClickListener, NavigationView.OnNavigationItemSelectedListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.start_activity)

        llProducts?.setOnClickListener(this)
        llExercises?.setOnClickListener(this)
        llSubscriptions?.setOnClickListener(this)
        llSessions?.setOnClickListener(this)
        llUsers?.setOnClickListener(this)
        llStatistics?.setOnClickListener(this)
        llTransactions?.setOnClickListener(this)



        val toolbar = findViewById<View>(R.id.my_toolbar) as Toolbar
        setSupportActionBar(toolbar)

        val navigationView=findViewById<View>(R.id.navigation) as NavigationView
        navigationView.setNavigationItemSelectedListener(this)



        val homeButton = findViewById<ImageButton>(R.id.homeButton)

        homeButton.setOnClickListener {
            if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
                drawerLayout.closeDrawer(GravityCompat.START)
            } else {
                drawerLayout.openDrawer(GravityCompat.START)
            }
        }
    }


    override fun onClick(view: View?) {
        when(view?.id){
            R.id.llProducts ->{
                navigateToActivity(AdminProductActivity::class.java)
            }
            R.id.llExercises ->{

            }
            R.id.llSubscriptions ->{

            }
            R.id.llSessions ->{

            }
            R.id.llUsers ->{

            }
            R.id.llStatistics ->{

            }
            R.id.llTransactions ->{

            }
        }
    }


    override fun onNavigationItemSelected(menuItem: MenuItem): Boolean {
        when(menuItem.itemId){

            R.id.nav_settings ->{

            }

            R.id.nav_logout ->{
                Toast.makeText(this,"logout is clicked", Toast.LENGTH_SHORT).show()
            }
        }
        drawerLayout.closeDrawer(GravityCompat.START)
        return true
    }

    private fun navigateToActivity(activity: Class<*>) {
        val intent = Intent(this, activity)
        startActivity(intent)
    }
}
