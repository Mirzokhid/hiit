package com.example.bob.hiit

import android.content.Intent
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.WindowManager
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.lifecycle.Observer
import com.example.bob.hiit.model.User
import com.example.bob.hiit.viewModel.MainViewModel
import kotlinx.android.synthetic.main.activity_register.*
import org.koin.android.ext.android.inject

class RegisterActivity : AppCompatActivity() {

    var userTypeState:Boolean?=null
    val viewModel: MainViewModel by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)
        changeStatusBarColor()


        initUserTypeSpinner()


        cirRegisterButton?.setOnClickListener {
            checkRegistrationValidation()
        }
    }


    fun changeStatusBarColor(){
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
            val window=window
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            window.statusBarColor=resources.getColor(R.color.register_bk_color)
        }
    }

    fun onLoginClick(View: View){
        val intent= Intent(this,LoginActivity::class.java)
        startActivity(intent)
        this.finish()
        overridePendingTransition(R.anim.slide_in_left,android.R.anim.slide_out_right)
    }


    private fun initUserTypeSpinner() {
        val adapter = ArrayAdapter(this, R.layout.row_spinner_lang, resources.getStringArray(R.array.user_type))
        userType.adapter = adapter
        adapter.setDropDownViewResource(R.layout.row_spinner_lang)

        // onItemClickListener
        val userTypes = resources.getStringArray(R.array.user_type).asList()
        userType?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                userTypeState =userTypes[position].equals("Trainer")
            }
        }
    }


    private fun checkRegistrationValidation(){

        val username=editTextName.text.toString()
        val password=editTextPassword.text.toString()

        if(username.length<6){
            editTextName.requestFocus()
            editTextName.setError("Email address should be at least 6 characters")
            return
        }

        if(password.isBlank()){
            editTextPassword.requestFocus()
            editTextPassword.setError("Password should not be an empty")
            return
        }


        val userRegistrationResponse=User()
        userRegistrationResponse.username=username
        userRegistrationResponse.password=password
        userRegistrationResponse.isTrainer=userTypeState

        viewModel.userRegistration(userRegistrationResponse,this)

        viewModel.isResponseSuccessful.observe(this, Observer<Boolean> { t ->
            if(t){
                Toast.makeText(this,"registered successful",Toast.LENGTH_SHORT).show()
                val intent=Intent(this@RegisterActivity,LoginActivity::class.java)
                startActivity(intent)
                this@RegisterActivity.finish()
            }
        })
    }
}
