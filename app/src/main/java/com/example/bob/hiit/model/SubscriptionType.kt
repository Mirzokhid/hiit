package com.example.bob.hiit.model

import com.google.gson.annotations.SerializedName

class SubscriptionTypeList {
    @SerializedName("count")
    var count: Int? = null

    @SerializedName("results")
    var subsList: ArrayList<SubscriptionType>? = null
}

class SubscriptionType {
    @SerializedName("id")
    var id: Int? = null

    @SerializedName("type")
    var type: String? = null

    @SerializedName("fee")
    var fee: Int? = null

    @SerializedName("created_date")
    var created_date: String? = null
}