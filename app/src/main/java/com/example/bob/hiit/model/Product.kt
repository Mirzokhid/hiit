package com.example.bob.hiit.model

import com.google.gson.annotations.SerializedName


class ProductResponse{

    @SerializedName("count")
    var count:Int?=null

    @SerializedName("results")
    var results:ArrayList<Product>?=null
}

class Product {

    @SerializedName("id")
    var productId:Int?=null

    @SerializedName("category")
    var category:Category?=null

    @SerializedName("title")
    var productTitle:String?=null

    @SerializedName("comment")
    var comment:String?=null

    @SerializedName("price")
    var price:String?=null

    @SerializedName("creator")
    var creator:Creator?=null

    @SerializedName("balance")
    var balance:Balance?=null

    @SerializedName("created_date")
    var createdDate:String?=null

}

class Category{

    @SerializedName("id")
    var categoryId:Int?=null

    @SerializedName("title")
    var title:String?=null

    @SerializedName("created_date")
    var createdDate:String?=null

}

class Creator{

    @SerializedName("id")
    var creatorId:Int?=null

    @SerializedName("first_name")
    var firstName:String?=null

    @SerializedName("last_name")
    var lastName:String?=null

}

class Balance{

    @SerializedName("available")
    var available:Int?=null
}
