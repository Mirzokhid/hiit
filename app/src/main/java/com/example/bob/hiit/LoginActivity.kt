package com.example.bob.hiit

import android.content.Intent
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.WindowManager
import android.widget.Toast
import androidx.appcompat.app.AppCompatDelegate
import androidx.lifecycle.Observer
import com.example.bob.hiit.model.User
import com.example.bob.hiit.utils.Settings
import com.example.bob.hiit.viewModel.MainViewModel
import kotlinx.android.synthetic.main.activity_login.*
import org.koin.android.ext.android.inject

class LoginActivity : AppCompatActivity() {

    val viewModel: MainViewModel by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
        }
        setContentView(R.layout.activity_login)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            val window = window
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            window.statusBarColor = resources.getColor(R.color.register_bk_color)
        }


        val isUserIn = Settings.getInstance(this).isUserIn
        if (isUserIn) {
            navigateToActivity(MainActivity::class.java)
        }



        cirLoginButton?.setOnClickListener {
            checkLoginValidation()
        }

    }

    fun onLoginClick(view: View) {
        val intent = Intent(this, RegisterActivity::class.java)
        startActivity(intent)
        this.finish()
        overridePendingTransition(R.anim.slide_in_right, R.anim.stay)
    }


    private fun checkLoginValidation() {

        val username = editTextEmail.text.toString()
        val password = editTextPassword.text.toString()

        if (username.length < 6) {
            editTextEmail.requestFocus()
            editTextEmail.setError("Email address should be at least 6 characters")
            return
        }
        if (password.isBlank()) {
            editTextPassword.requestFocus()
            editTextPassword.setError("Password should not be an empty")
            return
        }

        viewModel.userAuthentication(username, password, this)

        viewModel.registrationResponse.observe(this,
            Observer<User> { t ->
                Settings.getInstance(this).saveUserId(t.id!!)
                Settings.getInstance(this).saveUserToken(t.token)
                Settings.getInstance(this).saveUserName(username)
                if (t.firstName != null && t.lastName != null) {
                    Settings.getInstance(this).isUserIn = true
                    navigateToActivity(MainActivity::class.java)
                } else {
                    navigateToActivity(UpdateUserDetails::class.java)
                }
            })

    }

    private fun navigateToActivity(activity: Class<*>) {
        Toast.makeText(this, "logged in successfully", Toast.LENGTH_SHORT).show()
        val intent = Intent(this, activity)
        startActivity(intent)
        this.finish()
    }

}


