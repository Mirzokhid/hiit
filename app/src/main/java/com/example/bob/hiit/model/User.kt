package com.example.bob.hiit.model

import com.google.gson.annotations.SerializedName

class UserList {
    @SerializedName("count")
    var count: Int? = null

    @SerializedName("results")
    var userLists: ArrayList<User>? = null
}

class User {

    @SerializedName("id")
    var id: Int? = null

    @SerializedName("username")
    var username: String? = null

    @SerializedName("first_name")
    var firstName: String? = null

    @SerializedName("last_name")
    var lastName: String? = null

    @SerializedName("role")
    var role: String? = null

    @SerializedName("is_trainer")
    var isTrainer: Boolean? = null

    @SerializedName("token")
    var token: String? = null

    @SerializedName("password")
    var password: String? = null

    @SerializedName("date_of_birth")
    var dateOfBirth: String? = null

    @SerializedName("weight")
    var weight: Int? = null

    @SerializedName("height")
    var height: Int? = null


}

