package com.example.bob.hiit

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import com.example.bob.hiit.utils.Settings
import kotlinx.android.synthetic.main.activity_date.*
import kotlinx.android.synthetic.main.activity_subscription.*
import kotlinx.android.synthetic.main.activity_subscription.toolbar
import java.text.SimpleDateFormat
import java.util.*

class DateActivity : AppCompatActivity() {
    private var checkInDay = -1
    private var checkInMonth = -1
    private var checkInYear = -1
    private var date = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_date)

        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        val bundle = intent.extras
        val dateFormat = bundle!!.getString("date")

        checkCalendar.setOnDateChangeListener { view, year, month, dayOfMonth ->
            checkInDay = dayOfMonth
            checkInMonth = month
            checkInYear = year
        }

        saveBtn.setOnClickListener {
            val simpleDateFormat = SimpleDateFormat("yyyy-MM-dd")
            val date1 = Date(
                checkInYear - 1900,
                checkInMonth + 1,
                checkInDay
            )
            date = simpleDateFormat.format(date1)
            if (dateFormat.equals("beginDate"))
                Settings.getInstance(this).beginDate = date
            else if (dateFormat.equals("endDate"))
                Settings.getInstance(this).endDate = date

            this.finish()
        }

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            this.finish()
        }
        return true
    }
}
