package com.example.bob.hiit.dialogs

import android.app.DatePickerDialog
import android.app.Dialog
import android.os.Bundle
import androidx.fragment.app.DialogFragment
import android.widget.DatePicker
import java.util.*

class DatePickerFragment : DialogFragment(), DatePickerDialog.OnDateSetListener {
    interface OnDatePickedListener {
        fun onDatePicked(date: Date)
    }

    public var onDatePickedListener: OnDatePickedListener? = null
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        // Use the current date as the default date in the picker
        var bundle: Bundle? = arguments
        if (bundle != null && bundle.containsKey("preselectedDate")) {
            val c = Calendar.getInstance()
            c.time = bundle.getSerializable("preselectedDate") as Date
            val year = c.get(Calendar.YEAR)
            val month = c.get(Calendar.MONTH)
            val day = c.get(Calendar.DAY_OF_MONTH)
            return DatePickerDialog(activity!!, this, year, month, day)
        } else {
            val c = Calendar.getInstance()
            val year = 1990
            val month = c.get(Calendar.MONTH)
            val day = c.get(Calendar.DAY_OF_MONTH)

            return DatePickerDialog(activity!!, this, year, month, day)
        }

    }

    override fun onDateSet(view: DatePicker?, year: Int, month: Int, day: Int) {
        var calendar: Calendar = Calendar.getInstance()
        calendar.apply {
            set(Calendar.YEAR, year)
            set(Calendar.MONTH, month)
            set(Calendar.DAY_OF_MONTH, day)
        }
        onDatePickedListener?.onDatePicked(calendar.time)

    }

    public companion object {
        fun getInstance(onDatePickedListener: OnDatePickedListener): DatePickerFragment {
            val datePickerFragment = DatePickerFragment()
            datePickerFragment.onDatePickedListener = onDatePickedListener
            return datePickerFragment
        }

        fun getInstance(onDatePickedListener: OnDatePickedListener, date: Date): DatePickerFragment {
            val datePickerFragment = DatePickerFragment()
            datePickerFragment.onDatePickedListener = onDatePickedListener
            val bundle = Bundle().apply { putSerializable("preselectedDate", date) }
            datePickerFragment.arguments = bundle
            return datePickerFragment
        }
    }

}