package com.example.bob.hiit.adapters

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.bob.hiit.R
import com.example.bob.hiit.model.Subscription
import com.google.gson.Gson
import kotlinx.android.synthetic.main.subscription_list_item_view.view.*

class SubscriptionsAdapter(var subscriptions: ArrayList<Subscription>) :
    RecyclerView.Adapter<SubscriptionsAdapter.ViewHolder>() {

    private lateinit var listener: SubscriptionClickListener

    fun setListener(listener: SubscriptionClickListener) {
        this.listener = listener
    }


    override fun getItemCount(): Int = subscriptions.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val subscription = subscriptions[position]

        holder.itemView.tvSubscriptionType.text = subscription.subscriptionType!!.type
        holder.itemView.tvDate.text = "${subscription.beginDate} - ${subscription.endDate}"
        holder.itemView.tvUserName.text = subscription.member!!.firstName

        val bundle = Bundle()
        val subsJson = Gson().toJson(subscription)
        bundle.putString("subscription", subsJson)

        holder.itemView.setOnClickListener {
            listener.onSubsCLickListener(bundle)
        }

    }


    fun updateList(subs: ArrayList<Subscription>) {
        subscriptions = subs
        notifyDataSetChanged()
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View = LayoutInflater.from(parent.context)
            .inflate(R.layout.subscription_list_item_view, parent, false)
        return ViewHolder(view)
    }


    class ViewHolder(var view: View) : RecyclerView.ViewHolder(view)

    interface SubscriptionClickListener {
        fun onSubsCLickListener(bundle: Bundle)
    }
}