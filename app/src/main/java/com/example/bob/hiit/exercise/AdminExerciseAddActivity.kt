package com.example.bob.hiit.exercise

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.lifecycle.Observer
import com.example.bob.hiit.R
import com.example.bob.hiit.model.CategoryResponse
import com.example.bob.hiit.model.PostExercise
import com.example.bob.hiit.viewModel.MainViewModel
import kotlinx.android.synthetic.main.activity_exercises.exerciseToolbar
import kotlinx.android.synthetic.main.admin_add_exercise.*
import org.koin.android.ext.android.inject

class AdminExerciseAddActivity : AppCompatActivity(),  AdapterView.OnItemSelectedListener {

    private var selectedCategory = -1
    private lateinit var categoryList: ArrayList<String>
    private lateinit var categoryResponse: CategoryResponse
    private val viewModel: MainViewModel by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.admin_add_exercise)

        setSupportActionBar(exerciseToolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        categoryList = ArrayList()

        spExerciseCategoryList.onItemSelectedListener = this

        viewModel.getCategoryList(this)
        viewModel.categoryResponse.observe(this, Observer {
            if (it != null) {
                categoryResponse = it
                for (i in 0 until it.categories!!.size) {
                    categoryList.add(it.categories!![i].title!!)
                }
                if (categoryList.isNotEmpty()) {
                    val adapter = ArrayAdapter(
                        this,
                        android.R.layout.simple_spinner_item,
                        categoryList
                    ).also {
                        it.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                    }

                    spExerciseCategoryList.adapter = adapter
                }
            } else {
                Toast.makeText(this, "Error in getting Categories List", Toast.LENGTH_SHORT).show()
            }
        })

        addBtn.setOnClickListener {
            if (etExerciseName.text.isBlank()) {
                etExerciseName.error = "This field must not be empty"
                return@setOnClickListener
            }
            if (etExerciseDescription.text.isBlank()) {
                etExerciseDescription.error = "This field must not be empty"
                return@setOnClickListener
            }
            if (selectedCategory != -1){
                val exercise = PostExercise()
                exercise.category = selectedCategory
                exercise.title = etExerciseName.text.toString()
                exercise.description = etExerciseDescription.text.toString()

                viewModel.addExercise(this, exercise)
                viewModel.exercise.observe(this, Observer {
                    if(it!=null){
                        Toast.makeText(this, "Exercise successfully added", Toast.LENGTH_SHORT).show()
                        this.finish()
                    }
                    else
                        Toast.makeText(
                            this,
                            "Something went wrong, try again",
                            Toast.LENGTH_SHORT
                        ).show()
                })
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            this.finish()
        }
        return true
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {

    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        selectedCategory = categoryResponse.categories!![position].categoryId!!
    }
}
