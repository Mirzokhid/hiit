package com.example.bob.hiit.dialogs

import android.app.AlertDialog
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import com.example.bob.hiit.R
import kotlinx.android.synthetic.main.dialog_delete_exercise.*

class DeleteDialog(context: Context?) : AlertDialog(context) {

    private var dialogListener: DeleteExercise


    init {
        val v = LayoutInflater.from(context).inflate(R.layout.dialog_delete_exercise, null, false)
        this.dialogListener = context as DeleteExercise
        setView(v)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


        noBtn.setOnClickListener {
            dialogListener.noBtnClicked()
            dismiss()
        }

        yesBtn.setOnClickListener {
            dialogListener.yesBtnClicked()
            dismiss()
        }


    }

    interface DeleteExercise {

        fun yesBtnClicked()

        fun noBtnClicked()
    }
}