package com.example.bob.hiit.exercise

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.bob.hiit.R
import com.example.bob.hiit.adapters.ExercisesAdapter
import com.example.bob.hiit.dialogs.DeleteDialog
import com.example.bob.hiit.viewModel.MainViewModel
import kotlinx.android.synthetic.main.activity_exercises.*
import org.koin.android.ext.android.inject

class ExercisesActivity : AppCompatActivity(), ExercisesAdapter.ItemClickAndDeleteListener,
    DeleteDialog.DeleteExercise {

    private val viewModel: MainViewModel by inject()
    private lateinit var adapter: ExercisesAdapter
    private var selectedExerciseId = -1
    private var selectedExercisePos = -1
    private lateinit var dialog: DeleteDialog


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_exercises)

        setSupportActionBar(exerciseToolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        fabAddExercise.setOnClickListener {
            navigateToActivity(AdminExerciseAddActivity::class.java, null)
        }
    }

    override fun onResume() {
        super.onResume()

        viewModel.getAllExercises()
        viewModel.exercisesResponse.observe(this, Observer {
            if (it != null)
                if (it.count!! > 0) {
                    notFoundIcon.visibility = View.GONE
                    noResultMessage.visibility = View.GONE

                    adapter = ExercisesAdapter(it.exercises!!)
                    adapter.setListener(this)
                    adapter.notifyDataSetChanged()
                    rvExercises.adapter = adapter
                    rvExercises.layoutManager = LinearLayoutManager(this)
                } else {
                    notFoundIcon.visibility = View.VISIBLE
                    noResultMessage.visibility = View.VISIBLE
                }
        })
    }

    override fun onItemDeleteListener(id: Int, pos: Int) {
        selectedExerciseId = id
        selectedExercisePos = pos

        dialog = DeleteDialog(this)
        dialog.show()

    }

    override fun onItemClickListener(bundle: Bundle) {
        navigateToActivity(AdminExerciseDetail::class.java, bundle)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            this.finish()
        }
        return true
    }

    override fun yesBtnClicked() {
        viewModel.deleteExercise(this, selectedExerciseId)
        viewModel.isResponseSuccessful.observe(this, Observer {
            if (it)
                Toast.makeText(this, "Exercise is deleted", Toast.LENGTH_SHORT).show()
            else
                Toast.makeText(this, "Something went wrong", Toast.LENGTH_SHORT).show()
        })

        viewModel.getAllExercises()
        viewModel.exercisesResponse.observe(this, Observer {
            if (it != null) {
                adapter.updateList(it.exercises!!, selectedExercisePos)
                adapter.notifyItemChanged(selectedExercisePos)

                if (it.exercises!!.isEmpty()) {
                    notFoundIcon.visibility = View.VISIBLE
                    noResultMessage.visibility = View.VISIBLE
                } else {
                    notFoundIcon.visibility = View.GONE
                    noResultMessage.visibility = View.GONE
                }
            }

        })
    }

    override fun noBtnClicked() {
        dialog.dismiss()
    }

    private fun navigateToActivity(activity: Class<*>, bundle: Bundle?) {
        val intent = Intent(this, activity)
        if (bundle != null)
            intent.putExtras(bundle)
        startActivity(intent)
    }
}
