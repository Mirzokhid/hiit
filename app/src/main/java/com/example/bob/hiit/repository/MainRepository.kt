package com.example.bob.hiit.repository

import com.example.bob.hiit.api.ApiService
import com.example.bob.hiit.model.*
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.Path

interface MainRepository {

    fun userRegistration(user: User): Call<User>

    fun userAuthentication(username: String, password: String): Call<User>

    fun userUpdate(userId: Int, user: User): Call<User>

    fun getUserDetails(): Call<User>

    fun getProducts(): Call<ProductResponse>

    fun getCategoryList(): Call<CategoryResponse>

    fun getCategoryById(id: Int): Call<Category>

    fun getAllExercises(): Call<ExerciseResponse>

    fun addExercise(exercise: PostExercise): Call<Exercise>

    fun editExercise(id: Int, exercise: PostExercise): Call<Exercise>

    fun deleteExercise(id: Int): Call<Void>

    fun getAllUsers(): Call<UserList>

    fun getAllSubsType(): Call<SubscriptionTypeList>

    fun getAllSubs(): Call<SubscriptionsList>

    fun addNewSub(@Body sub: SubscriptionPost): Call<Void>

    fun getSubDetailById(@Path("id") id: Int): Call<Subscription>
}

class MainRepositoryImplementation(private val apiService: ApiService) : MainRepository {

    override fun getAllUsers(): Call<UserList> {
        return apiService.getAllUsers()
    }

    override fun getAllSubsType(): Call<SubscriptionTypeList> {
        return apiService.getAllSubsType()
    }

    override fun getAllSubs(): Call<SubscriptionsList> {
        return apiService.getAllSubs()
    }

    override fun addNewSub(sub: SubscriptionPost): Call<Void> {
        return apiService.addNewSub(sub)
    }

    override fun getSubDetailById(id: Int): Call<Subscription> {
        return apiService.getSubDetailById(id)
    }

    override fun getUserDetails(): Call<User> {
        return apiService.getUserDetails()
    }

    override fun userAuthentication(username: String, password: String): Call<User> {
        return apiService.userAuthentication(username, password)
    }

    override fun userRegistration(user: User): Call<User> {
        return apiService.userRegistration(user)
    }

    override fun userUpdate(userId: Int, user: User): Call<User> {
        return apiService.userUpdate(userId, user)
    }

    override fun getProducts(): Call<ProductResponse> {
        return apiService.getProducts()
    }

    override fun getCategoryList(): Call<CategoryResponse> {
        return apiService.getCategoryList()
    }

    override fun getCategoryById(id: Int): Call<Category> {
        return apiService.getCategoryById(id)
    }

    override fun getAllExercises(): Call<ExerciseResponse> {
        return apiService.getAllExercises()
    }

    override fun addExercise(exercise: PostExercise): Call<Exercise> {
        return apiService.addExercise(exercise)
    }

    override fun editExercise(id: Int, exercise: PostExercise): Call<Exercise> {
        return apiService.editExercise(id, exercise)
    }

    override fun deleteExercise(id: Int): Call<Void> {
        return apiService.deleteExercise(id)
    }
}