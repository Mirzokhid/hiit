package com.example.bob.hiit.subscription

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.lifecycle.Observer
import com.example.bob.hiit.DateActivity
import com.example.bob.hiit.R
import com.example.bob.hiit.model.Subscription
import com.example.bob.hiit.model.SubscriptionPost
import com.example.bob.hiit.model.SubscriptionTypeList
import com.example.bob.hiit.model.UserList
import com.example.bob.hiit.utils.Settings
import com.example.bob.hiit.viewModel.MainViewModel
import kotlinx.android.synthetic.main.activity_subscription.*
import kotlinx.android.synthetic.main.activity_subscription.toolbar
import kotlinx.android.synthetic.main.activity_subscription_add.*
import org.koin.android.ext.android.inject

class SubscriptionAddActivity : AppCompatActivity(), AdapterView.OnItemSelectedListener {

    private val viewModel: MainViewModel by inject()
    private lateinit var subsTypeList: ArrayList<String>
    private lateinit var memberList: ArrayList<String>
    private lateinit var paymentList: ArrayList<String>

    private lateinit var subsTypeResponse: SubscriptionTypeList
    private lateinit var memberResponse: UserList

    private var selectedSubType = -1
    private var selectedMember = -1
    private var selectedPayment = ""
    private var startDate = ""
    private var endDateText = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_subscription_add)

        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        Settings.getInstance(this).beginDate = "-"
        Settings.getInstance(this).endDate = "-"

        subsTypeList = ArrayList()
        memberList = ArrayList()
        paymentList = ArrayList()

        paymentList.add("cash")
        paymentList.add("Credit Card")

        spSubsTypeList.onItemSelectedListener = this
        spMemberList.onItemSelectedListener = this
        spPaymentTypeList.onItemSelectedListener = this

        var adapter = ArrayAdapter(
            this,
            android.R.layout.simple_spinner_item,
            paymentList
        ).also {
            it.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        }

        spPaymentTypeList.adapter = adapter

        viewModel.getAllSubsType()
        viewModel.subscriptionTypeList.observe(this, Observer {
            if (it != null) {
                subsTypeResponse = it
                for (i in 0 until it.subsList!!.size)
                    subsTypeList.add(it.subsList!![i].type!!)
                if (subsTypeList.isNotEmpty()) {

                    adapter = ArrayAdapter(
                        this,
                        android.R.layout.simple_spinner_item,
                        subsTypeList
                    ).also {
                        it.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                    }

                    spSubsTypeList.adapter = adapter
                }
            } else {
                Toast.makeText(this, "Error in getting Subscriptions Type List", Toast.LENGTH_SHORT)
                    .show()
            }
        })

        viewModel.getAllUsers()
        viewModel.usersList.observe(this, Observer {
            if (it != null) {
                memberResponse = it
                for (i in 0 until it.userLists!!.size)
                    memberList.add(it.userLists!![i].firstName!!)
                if (memberList.isNotEmpty()) {
                    adapter = ArrayAdapter(
                        this,
                        android.R.layout.simple_spinner_item,
                        memberList
                    ).also {
                        it.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                    }

                    spMemberList.adapter = adapter
                }
            } else {
                Toast.makeText(this, "Error in getting Users  List", Toast.LENGTH_SHORT).show()
            }
        })
        val bundle = Bundle()
        beginDate.setOnClickListener {
            bundle.putString("date", "beginDate")
            navigateToActivity(DateActivity::class.java, bundle)
        }

        endDate.setOnClickListener {
            bundle.putString("date", "endDate")
            navigateToActivity(DateActivity::class.java, bundle)
        }

        addBtn.setOnClickListener {
            if (startDate != "-" && endDateText != "-" && selectedMember != -1 && selectedPayment != "-" && selectedSubType != -1) {
                val sub = SubscriptionPost()
                sub.subscriptionType = selectedSubType
                sub.paymentType = selectedPayment
                sub.member = selectedMember
                sub.beginDate = startDate
                sub.endDate = endDateText

                viewModel.addNewSub(this, sub)
                viewModel.isResponseSuccessful.observe(this, Observer {
                    if (it) {
                        Toast.makeText(this, "Subscription successfully added", Toast.LENGTH_SHORT)
                            .show()
                        this.finish()

                    } else
                        Toast.makeText(
                            this,
                            "Something went wrong, try again",
                            Toast.LENGTH_SHORT
                        ).show()
                })
            } else
                Toast.makeText(this, "All fields must be filled up", Toast.LENGTH_SHORT).show()
        }


    }

    override fun onResume() {
        super.onResume()
        startDate = Settings.getInstance(this).beginDate
        endDateText = Settings.getInstance(this).endDate

        beginDate.text = startDate
        endDate.text = endDateText
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            this.finish()
        }
        return true
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {

    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        when (parent!!.id) {
            R.id.spPaymentTypeList -> {
                selectedPayment = paymentList[position]
            }
            R.id.spSubsTypeList -> {
                selectedSubType = subsTypeResponse.subsList!![position].id!!
            }
            R.id.spMemberList -> {
                selectedMember = memberResponse.userLists!![position].id!!
            }
        }
    }

    private fun navigateToActivity(activity: Class<*>, bundle: Bundle?) {
        val intent = Intent(this, activity)
        if (bundle != null)
            intent.putExtras(bundle)
        startActivity(intent)
    }
}
