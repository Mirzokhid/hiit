package com.example.bob.hiit.model

import com.google.gson.annotations.SerializedName

class ExerciseResponse {
    @SerializedName("count")
    var count: Int? = null

    @SerializedName("results")
    var exercises: ArrayList<Exercise>? = null
}

class Exercise {
    @SerializedName("id")
    var id: Int? = null

    @SerializedName("title")
    var title: String? = null

    @SerializedName("description")
    var description: String? = null

    @SerializedName("category")
    var category: Category? = null

    @SerializedName("files")
    var files: ArrayList<String>? = null

    @SerializedName("created_date")
    var createdDate: String? = null

}

class PostExercise {

    @SerializedName("title")
    var title: String? = null

    @SerializedName("description")
    var description: String? = null

    @SerializedName("category")
    var category: Int? = null
}