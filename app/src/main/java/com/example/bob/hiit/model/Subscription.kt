package com.example.bob.hiit.model

import com.google.gson.annotations.SerializedName

class SubscriptionsList {
    @SerializedName("count")
    var count: Int? = null

    @SerializedName("results")
    var subsList: ArrayList<Subscription>? = null
}

class Subscription {
    @SerializedName("id")
    var id: Int? = null

    @SerializedName("created_date")
    var createdDate: String? = null

    @SerializedName("modified_date")
    var modifiedDate: String? = null

    @SerializedName("is_delete")
    var isDelete: Boolean? = null

    @SerializedName("begin")
    var beginDate: String? = null

    @SerializedName("end")
    var endDate: String? = null

    @SerializedName("active")
    var active: Boolean? = null

    @SerializedName("payment_type")
    var paymentType: String? = null

    @SerializedName("subscription_type")
    var subscriptionType: SubscriptionType? = null

    @SerializedName("member")
    var member: User? = null

    @SerializedName("trainer")
    var trainer: User? = null

}

class SubscriptionPost {
    @SerializedName("subscription_type")
    var subscriptionType: Int? = null

    @SerializedName("begin")
    var beginDate: String? = null

    @SerializedName("end")
    var endDate: String? = null

    @SerializedName("member")
    var member: Int? = null

    @SerializedName("payment_type")
    var paymentType: String? = null

}