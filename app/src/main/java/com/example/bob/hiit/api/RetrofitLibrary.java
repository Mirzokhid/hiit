package com.example.bob.hiit.api;

import android.content.Context;

import com.example.bob.hiit.utils.Settings;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitLibrary {



    public Retrofit initClient(final Context context){

        final Settings sharedPreferences= Settings.getInstance(context);
        HttpLoggingInterceptor logging=new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient client=new OkHttpClient.Builder()
                .connectTimeout(5, TimeUnit.SECONDS)
                .readTimeout(10,TimeUnit.SECONDS)
                .writeTimeout(10,TimeUnit.SECONDS)
                .addInterceptor(logging)
                .addInterceptor(new Interceptor() {
                    @Override
                    public Response intercept(Chain chain) throws IOException {
                        Request request;
                        if(sharedPreferences.getUserToken()!=null){
                            request=chain.request()
                                    .newBuilder()
                                    .addHeader("Authorization","Token "+sharedPreferences.getUserToken())
                                    .addHeader("Content-Type","application/json")
                                    .build();
                        }else{
                            request=chain.request();
                        }
                        return chain.proceed(request);
                    }
                }).build();

        return new Retrofit.Builder()
                .baseUrl("http://keelo.herokuapp.com")
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

    }
}
