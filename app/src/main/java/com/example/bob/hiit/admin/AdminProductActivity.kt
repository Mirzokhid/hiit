package com.example.bob.hiit.admin

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.bob.hiit.R
import com.example.bob.hiit.adapters.ProductAdapter
import com.example.bob.hiit.viewModel.MainViewModel
import kotlinx.android.synthetic.main.activity_admin_product.*
import kotlinx.android.synthetic.main.admin_product_list.*
import org.koin.android.ext.android.inject

    class AdminProductActivity : AppCompatActivity() {

    val viewModel:MainViewModel by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_admin_product)

        viewModel.getProducts(this)

        viewModel.productResponseResult.observe(this, Observer {
            rvProducts.layoutManager=LinearLayoutManager(this)
            val productAdapter=ProductAdapter()
            rvProducts.adapter=productAdapter
            productAdapter.updateList(it.results!!)

        })


    }
}
