package com.example.bob.hiit.subscription

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import com.example.bob.hiit.R
import com.example.bob.hiit.model.Subscription
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_subscription_detail.*
import kotlinx.android.synthetic.main.activity_subscription_detail.toolbar

class SubscriptionDetailActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_subscription_detail)

        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        val bundle = intent.extras

        val subsJson = bundle!!.getString("subscription")

        val subsc = Gson().fromJson(subsJson, Subscription::class.java)

        subsType.text = subsc.subscriptionType!!.type
        beginDate.text = subsc.beginDate
        endDate.text = subsc.endDate
        paymentTypeText.text = subsc.paymentType
        member.text = subsc.member!!.firstName
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            this.finish()
        }
        return true
    }
}
