package com.example.bob.hiit.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.bob.hiit.R
import com.example.bob.hiit.model.Product

class ProductAdapter() :RecyclerView.Adapter<ProductAdapter.ViewHolder>() {

    var products:ArrayList<Product>?=null

    override fun getItemCount(): Int=products!!.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val product=products!!.get(position)

        holder.tvProductTitle.text=product.productTitle

    }



    fun updateList(items: ArrayList<Product>) {
        products = items
        notifyDataSetChanged()
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View = LayoutInflater.from(parent.context).inflate(R.layout.product_recycler_item, parent, false)
        return ViewHolder(view)
    }


    class ViewHolder(var view:View):RecyclerView.ViewHolder(view){
        var ivProduct:ImageView=view.findViewById(R.id.ivProduct)
        var tvProductTitle:TextView=view.findViewById(R.id.tvProductTitle)

    }
}