package com.example.bob.hiit

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.core.view.GravityCompat
import androidx.lifecycle.Observer
import com.example.bob.hiit.admin.AdminProductActivity
import com.example.bob.hiit.exercise.ExercisesActivity
import com.example.bob.hiit.subscription.SubscriptionActivity
import com.example.bob.hiit.utils.Settings
import com.example.bob.hiit.viewModel.MainViewModel
import kotlinx.android.synthetic.main.activity_admin_home.*
import kotlinx.android.synthetic.main.start_activity.*
import org.koin.android.ext.android.inject

class MainActivity : AppCompatActivity(), View.OnClickListener {

    private val viewModel: MainViewModel by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.start_activity)

        viewModel.getUserDetails()
        viewModel.user.observe(this, Observer {
            if (it != null) {
                companyName.text = "${it.firstName} ${it.lastName}"
            }
        })

        setSupportActionBar(my_toolbar)

        llProducts?.setOnClickListener(this)
        llExercises?.setOnClickListener(this)
        llSubscriptions?.setOnClickListener(this)
        llSessions?.setOnClickListener(this)
        llUsers?.setOnClickListener(this)
        llStatistics?.setOnClickListener(this)
        llTransactions?.setOnClickListener(this)

        navigation.setNavigationItemSelectedListener {
            when (it.itemId) {

                R.id.nav_settings -> {

                }

                R.id.nav_logout -> {
                    Settings.getInstance(this).clear()
                    navigateToActivity(LoginActivity::class.java)
                    this.finish()
                    Toast.makeText(this, "Logged Out", Toast.LENGTH_SHORT).show()
                }
            }

            drawerLayout.closeDrawer(GravityCompat.START)
            return@setNavigationItemSelectedListener true
        }

        homeButton.setOnClickListener {
            drawerLayout.openDrawer(GravityCompat.START)
        }

    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.llProducts -> {
                navigateToActivity(AdminProductActivity::class.java)
            }
            R.id.llExercises -> {
                navigateToActivity(ExercisesActivity::class.java)
            }
            R.id.llSubscriptions -> {
                navigateToActivity(SubscriptionActivity::class.java)
            }
            R.id.llSessions -> {

            }
            R.id.llUsers -> {

            }
            R.id.llStatistics -> {

            }
            R.id.llTransactions -> {

            }
        }
    }

    private fun navigateToActivity(activity: Class<*>) {
        val intent = Intent(this, activity)
        startActivity(intent)
    }


}
