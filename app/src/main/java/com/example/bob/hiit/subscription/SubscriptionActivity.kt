package com.example.bob.hiit.subscription

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.bob.hiit.R
import com.example.bob.hiit.adapters.SubscriptionsAdapter
import com.example.bob.hiit.viewModel.MainViewModel
import kotlinx.android.synthetic.main.activity_exercises.*
import kotlinx.android.synthetic.main.activity_subscription.*
import org.koin.android.ext.android.inject

class SubscriptionActivity : AppCompatActivity(), SubscriptionsAdapter.SubscriptionClickListener {

    private val viewModel: MainViewModel by inject()
    private lateinit var adapter: SubscriptionsAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_subscription)

        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        fabAddSubscription.setOnClickListener {
            navigateToActivity(SubscriptionAddActivity::class.java, null)
        }


    }

    override fun onResume() {
        super.onResume()

        viewModel.getAllSubs()
        viewModel.subscriptionsList.observe(this, Observer {
            if (it != null) {
                notFoundIcon1.visibility = View.GONE
                noResultMessage1.visibility = View.GONE
                adapter = SubscriptionsAdapter((it.subsList!!))
                adapter.setListener(this)
                rvSubscription.adapter = adapter
                rvSubscription.layoutManager = LinearLayoutManager(this)
            } else {
                notFoundIcon1.visibility = View.VISIBLE
                noResultMessage1.visibility = View.VISIBLE
            }
        })

    }

    override fun onSubsCLickListener(bundle: Bundle) {
        navigateToActivity(SubscriptionDetailActivity::class.java, bundle)
    }

    private fun navigateToActivity(activity: Class<*>, bundle: Bundle?) {
        val intent = Intent(this, activity)
        if (bundle != null)
            intent.putExtras(bundle)
        startActivity(intent)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            this.finish()
        }
        return true
    }
}
