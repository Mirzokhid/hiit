package com.example.bob.hiit.adapters

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.bob.hiit.R
import com.example.bob.hiit.model.Exercise
import com.example.bob.hiit.model.Product
import com.google.gson.Gson
import kotlinx.android.synthetic.main.product_recycler_item.view.*

class ExercisesAdapter(private var exercises: ArrayList<Exercise>) :
    RecyclerView.Adapter<ExercisesAdapter.ViewHolder>() {

    private lateinit var listener: ItemClickAndDeleteListener

    fun setListener(l: ItemClickAndDeleteListener) {
        listener = l
    }


    override fun getItemCount(): Int = exercises.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val exercise = exercises[position]

        holder.tvProductTitle.text = exercise.title
        holder.itemView.deleteBtn.setOnClickListener {
            listener.onItemDeleteListener(exercise.id!!, position)
        }

        val bundle = Bundle()
        val exerciseJson = Gson().toJson(exercise)
        bundle.putString("exercise", exerciseJson)

        holder.itemView.setOnClickListener {
            listener.onItemClickListener(bundle)
        }
    }


    fun updateList(items: ArrayList<Exercise>, pos:Int) {
        exercises = items
        notifyItemChanged(pos)
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View = LayoutInflater.from(parent.context)
            .inflate(R.layout.product_recycler_item, parent, false)
        return ViewHolder(view)
    }


    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var tvProductTitle: TextView = view.findViewById(R.id.tvProductTitle)
    }

    interface ItemClickAndDeleteListener {

        fun onItemDeleteListener(id: Int, pos:Int)

        fun onItemClickListener(bundle: Bundle)
    }
}