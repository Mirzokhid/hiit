package com.example.bob.hiit

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.view.View
import android.view.WindowManager
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.example.bob.hiit.dialogs.DatePickerFragment
import com.example.bob.hiit.model.User
import com.example.bob.hiit.utils.Settings
import com.example.bob.hiit.viewModel.MainViewModel
import kotlinx.android.synthetic.main.activity_update_user_details.*
import org.koin.android.ext.android.inject
import java.text.SimpleDateFormat
import java.util.*

class UpdateUserDetails : AppCompatActivity(), DatePickerFragment.OnDatePickedListener {

    private var birthDateCalendar: Date? = null
    private var dateOfBirth: String? = null
    val viewModel: MainViewModel by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_update_user_details)


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            val window = window
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            window.statusBarColor = resources.getColor(R.color.register_bk_color)
        }


        editTextDate?.setOnClickListener {
            onBirthDayClick()
        }

        cirSaveButton?.setOnClickListener {
            checkValidation()
        }

    }

    private fun checkValidation() {
        val firstName = editTextFirstName.text.toString()
        val lastName = editTextLastName.text.toString()
        val dateOfBirth = editTextDate.text.toString()
        val weight = editTextWeight.text.toString()
        val height = editTextHeight.text.toString()
        val userName = Settings.getInstance(this).userName

        if (firstName.isBlank()) {
            editTextFirstName.requestFocus()
            editTextFirstName.setError("Name should not be empty")
            return
        }

        if (lastName.isBlank()) {
            editTextLastName.requestFocus()
            editTextLastName.setError("Last name should not be empty")
            return
        }

        if (dateOfBirth.isBlank()) {
            editTextDate.requestFocus()
            editTextDate.setError("Date of birth should not be empty")
        }

        if (weight.isBlank()) {
            editTextWeight.requestFocus()
            editTextWeight.setError("Weight should not be empty")
            return
        }

        if (height.isBlank()) {
            editTextHeight.requestFocus()
            editTextHeight.setError("Height should not be empty")
            return
        }


        val userCredentials = User()
        userCredentials.firstName = firstName
        userCredentials.lastName = lastName
        userCredentials.dateOfBirth = dateOfBirth
        userCredentials.username = userName
        userCredentials.weight = weight.toInt()
        userCredentials.height = height.toInt()

        val userId = Settings.getInstance(this).userId
        viewModel.userUpdate(userId, userCredentials, this)


        viewModel.isResponseSuccessful.observe(this, Observer<Boolean> { t ->
            if (t != null && t) {
                Settings.getInstance(this).isUserIn = true
                startActivity(Intent(this, MainActivity::class.java))
                this.finish()
            } else {
                Toast.makeText(this, "Something went wrong", Toast.LENGTH_SHORT).show()
            }
        })
    }


    fun onBirthDayClick() {
        val fragment: DatePickerFragment
        if (birthDateCalendar != null) {
            fragment = DatePickerFragment.Companion.getInstance(this, birthDateCalendar!!)
        } else {
            fragment = DatePickerFragment.Companion.getInstance(this)

        }
        fragment.show(supportFragmentManager, "datePicker")
    }

    override fun onDatePicked(date: Date) {
        birthDateCalendar = date
        dateOfBirth = getStringFromDate(birthDateCalendar!!)
        updateBirthdate()
    }

    private fun updateBirthdate() {
        if (birthDateCalendar != null) {
            editTextDate.setText(getSimpleDate(birthDateCalendar!!))
        }
    }


    private fun getStringFromDate(date: Date): String {
        val dateFormat = SimpleDateFormat("yyyy-MM-dd")
        return dateFormat.format(date)
    }

    private fun getSimpleDate(date: Date): String {
        val dateFormat = SimpleDateFormat("yyyy-MM-dd")
        return dateFormat.format(date)
    }
}
